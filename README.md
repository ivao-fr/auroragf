# Aurora '*Guyane Française*' Sector file

## Content

This project contains the whole Aurora Sector files for the French Guyana (SOOO).

For latest updates, see the [Changelog](CHANGELOG.md)

## Release

The folder structure of the project is similar to the one found inside Aurora installation folder. Though, to release the sector files on the IVAO server, a zip-archive, with a slightly different architecture has to be generated.

This archive can be generated using the [generate-zip.cmd](Release/generate-zip.cmd) script file. As Aurora is for now only Windows compatible, this script is only runnable on a Windows (32 or 64 bits) platform.

The script uses the [7-Zip](http://www.7-zip.org) portable 32bit binaries for Windows. 7-Zip is distributed under a GNU LGPL [license](https://www.7-zip.org/license.txt).

The generated zip archive has the folowing structure :
```
sooo.zip
   │── ColorScheme1.clr
   │── ColorScheme2.clr
   │   
   │── SOOO.isc
   │
   └───Include
       └───SOOO
           │── Changelog.txt
           └── ...
```

## Release Procedure

1. Wrap up the Changelog and commit it with reference to the `Sector Updates after AIRAC 2000 Release` issue.

2. Merge `Master` in `Main_Dev`, then `Main_Dev` into `Master`. Select **No fast forward** -option for merging.

3. Checkout `Master` and create release tag : `Release-AIRAC-2001`.

4. Close all resolved issues in GitLab with comment : `Released in tag [Release-AIRAC-2001](../../tags/Release-AIRAC-2001)`.

5. Create `Sector Updates after AIRAC 2001 Release` issue.

6. Using GitLab, manually go edit the release notes of the tag adding the content of the Changelog using markdown symbology.

7. In `Master`, create the zip-archive with the help of the cmd-tool.

8. Officially release the update via IVAO-channels.

