# Aurora Guyane Francaise Changelog #

## Update 07 February 2025 ##
**{+ AIRAC : 2501 +}**

### Updated Files ###

- **FIXES files (fix)** : SOOO
- **VOR files (vor)** : SOOO
- **NDB files (ndb)** : SOOO
- **AWY files (hawy/lawy)** : SOOO
- **APT files (apt)** : SOOO
- **RWY files (rwy)** : SOOO
- **STAR files (str)** : SOCA
- **Colorscheme (clr)** : FRANCE

---
### Added Files ###

- **Symbols file (sym)** : FR
- **FIXES files (fix)** : SOOO_Spec
- **SID files (sid)** : SOCH
- **STAR files (str)** : SOCH

---
---

## Update 19 June 2024 ##
**{+ AIRAC : 2406 +}**

### Updated Files ###

- **FIXES files (fix)** : SOOO
- **VOR files (vor)** : SOOO
- **NDB files (ndb)** : SOOO
- **AWY files (hawy/lawy)** : SOOO
- **APT files (apt)** : SOOO
- **RWY files (rwy)** : SOOO
- **ATC File (atc)** : SOOO
- **STAR files (str)** : SOCA
- **SID files (sid)** : SOCA
- **Prohibit files (prohibit)** : SOOO
- **Restricted files (restrict)** : SOOO
- **GND files (gnd)** : SOCA, SOGS, SOOA, SOOG, SOOM, SOOS
- **Taxiway label files (txi)** : SOCA
- **Gates label files (gts)** : SOCA
- **GEO files (geo)** : SOOO

---
### Added Files ###

- **ATIS file (atis)** : France
- **Danger files (danger)** : SOOO
- **GND files (gnd)** : SOOC, SOOR
- **Gates label files (gts)** : SOOA
- **Airspace Polygon files (tfl)** : SOOO

---
---
## Update 31 January 2021 ##
**{+ AIRAC : 2101 +}**

**No Applicable Updates to date**

---
---
## Update 17 November 2020 ##
**{+ AIRAC : 2012 +}**

**No Applicable Updates to date**