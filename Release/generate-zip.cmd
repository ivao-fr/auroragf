@echo off
setlocal

REM Deal with folders, try to understand where we are...
SET scriptfolder=%~dp0
FOR %%a IN ("%scriptfolder:~0,-1%") DO SET rootfolder=%%~dpa
IF %rootfolder:~-1%==\ SET rootfolder=%rootfolder:~0,-1%

REM Set local variables
SET zipfilename=sooo.zip
SET abszipfilename=%rootfolder%\Release\%zipfilename%
SET zipcommand=%rootfolder%\Release\7za

REM Remove previous release, if any.
del /f /q %abszipfilename% > nul 2>&1

REM Speak to the user
ECHO ----------------------------------------------
ECHO   Generating the %zipfilename% archive
ECHO ----------------------------------------------

REM Create the archive
%zipcommand% a -tzip %abszipfilename% %rootfolder%\SectorFiles\*.isc %rootfolder%\SectorFiles\Include %rootfolder%\CHANGELOG.md %rootfolder%\ColorSchemes\*.clr
REM Move and rename the Changelog into the correct folder
%zipcommand% rn %abszipfilename% CHANGELOG.md Include\SOOO\Changelog.txt > nul

REM Let the user understand what just damn happened
ECHO.
PAUSE

REM And quit! Love <3